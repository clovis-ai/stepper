
const int bufferSize = 200;
unsigned long ringBuffer[bufferSize];
int head = -1;
int tail = -1;

void writeBuffer(unsigned long data){
  if(data > 30000) data = 30000;
  ringBuffer[++head] = data; //écrire puis avancer
  Serial.print(F("Buffer : ["));
  Serial.print(head);
  Serial.print(F("]  < "));
  Serial.println(data);
  if(head >= bufferSize-1)
    head = -1;
}

unsigned long readBuffer(){
  if(tail == head){
    Serial.print(F("Buffer : ["));
    Serial.print(tail);
    Serial.println(F("] >  Fin atteinte"));
    return -1;
  }else{
    ++tail; //avancer puis écrire
    if(tail > bufferSize)
      tail = 0;
    Serial.print(F("Buffer : ["));
    Serial.print(tail);
    Serial.print(F("] >  "));
    Serial.println(ringBuffer[tail]);
    return ringBuffer[tail];
  }
}

void printBuffer(){
  for(int i = 0; i < bufferSize; ++i){
    Serial.print(ringBuffer[i]);
    Serial.print(" ");
  }
  Serial.println();
}

