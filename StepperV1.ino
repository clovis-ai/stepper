
/*
 * 
 * 
 * PINS RESERVEES
 * 6    SD Card I/O
 * 7    Wifi shield handshake
 * 10 \
 * 11 | WiFi shield & SD card I/O
 * 12 |
 * 13 /
 * 
 */



/*                                   MAIN                                               */

// Pin du capteur
const int capteurPin = 8;

// Pin de la carte SD
const int sdPin = 6;

// Valeurs

void setup() {
  pinMode(4, OUTPUT);
  digitalWrite(4, HIGH);
  
  // Démarrer la connexion des logs
  Serial.begin(9600);
  Serial.flush();
  Serial.println(F("Booting up ..."));
  
  // Créer la liaison avec le capteur
  pinMode(capteurPin, INPUT);

  // Démarrer le chronomètre
  startChrono();

  // Créer un nouveau fichier de sauvegarde
  //newFile();

  // Initialisation de la connexion
  beginWiFi();

  Serial.println(F("Ready ..."));
}

void loop() {
  // S'occuper du capteur
  act();
  connexionRead();
}

int i = 0;
boolean fallthrough = false;

void act(){
  // Attendre que l'aimant passe devant le capteur
  // (vérification toutes les millisecondes)
  i = 0;
  if(!fallthrough)
    while(digitalRead(capteurPin) == HIGH){delay(1); i++; if(i > 1000) return;}

  //                       F R O N T     D E S C E N D A N T
  
  // Arrêter le chronomètre et envoyer le temps au module I/O
  if(!fallthrough)
    newData(stopChrono());

  // Attendre que l'aimant ne soit plus devant le capteur
  // (vérification toutes les millisecondes)
  i = 0;
  fallthrough = false;
  while(digitalRead(capteurPin) == LOW){delay(1); i++; if(i > 1000){ fallthrough = true; return; }}

  //                         F R O N T     M O N T A N T

  // Relancer le chronomètre
  startChrono();
  delay(100);
}



void endProgram(){
  Serial.println(F("Order has been made to terminate."));

  // Fin du programme
  while(true);
}



String concat(String first, String second){
  first.concat(second);
  return first;
}

