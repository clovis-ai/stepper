#include <WiFi.h>
#include <SPI.h>
/*#include <iostream>
#include <string>

using namespace std;*/


/*
 * INITIALISATION
 * Initialiser cas
 * Si WiFi:
 *   Initialiser ssid
 *   Si WEP ou WPA : Initialiser pass
 */

// Cas possibles
enum Cas{
  WIFI_NO_ENCRYPTION,
  WIFI_WPA,
  WIFI_WEP,
  ETHER
};

Cas cas = WIFI_WPA;

WiFiServer server(80);

void beginWiFi(){
  const char ssid[] = "MiniStepper";            // Nom du réseau
  const char pass[] = "Test1234";             // Code ou clef
  const int keyIndex = 0;                       // Network key
  int statut = WL_IDLE_STATUS;
  
  // S'il n'y a pas de WiFi shield
  if(WiFi.status() == WL_NO_SHIELD){
    Serial.println(F("The WiFi shield can't be found ..."));
    endProgram();
  }

  Serial.print(F("WiFi firmware version : "));
  Serial.println (WiFi.firmwareVersion());

  // WiFi demandé
  Serial.print(F("Targetting network : "));
  Serial.println(ssid);

  // Méthode de connexion demandée
  Serial.print(F("Targetting encryption : "));
  switch(cas){
    case WIFI_NO_ENCRYPTION:
      Serial.println(F("no encryption"));
      break;
    case WIFI_WPA:
      Serial.println(F("WPA"));
      break;
    case WIFI_WEP:
      Serial.println(F("WEP"));
      break;
  }
  
  // Connexion ...
  int essai = 1;
  while(statut != WL_CONNECTED){
    Serial.print(F("Trying to connect to network : try #"));
    Serial.println(String(essai));
    switch(cas){
      case WIFI_NO_ENCRYPTION:
        statut = WiFi.begin(ssid);
        break;
      case WIFI_WPA:
        statut = WiFi.begin(ssid, pass);
        break;
      case WIFI_WEP:
        statut = WiFi.begin(ssid, keyIndex, pass);
        break;
      case ETHER:
        break;
      default:
        Serial.println(F("Impossible connexion type ..."));
        endProgram();
    }

    // Attendre de voir si c'est réussi (10 secondes)
    delay(10000);
    essai++;
  }
  Serial.println(F("Connexion successful !"));
  
  switch(cas){
    case WIFI_NO_ENCRYPTION:
    case WIFI_WPA:
    case WIFI_WEP:
      Serial.print(F("SSID : "));
      Serial.println(WiFi.SSID());
      byte bssid[6];
      WiFi.BSSID(bssid);
      Serial.print(F("BSSID : "));
      Serial.print(String(ssid[5], HEX)); Serial.print(":");
      Serial.print(String(ssid[4], HEX)); Serial.print(":");
      Serial.print(String(ssid[3], HEX)); Serial.print(":");
      Serial.print(String(ssid[2], HEX)); Serial.print(":");
      Serial.print(String(ssid[1], HEX)); Serial.print(":");
      Serial.println(String(ssid[0], HEX));
      Serial.print(F("Signal strength (RSSI) : ")); Serial.print(String(WiFi.RSSI())); Serial.println(F(" dBm"));
      Serial.println("Encryption type : "+String(WiFi.encryptionType(), HEX));
      break;
    case ETHER:
      break;
    default:
      Serial.println(F("Impossible connexion type ..."));
      endProgram();
  }

  delay(1000);
  // Announcing IP
  Serial.println(" ");
  IPAddress ip = WiFi.localIP();
  Serial.print("Adresse IP : "); Serial.print(String(ip[0], DEC)); Serial.print(".");
                                 Serial.print(String(ip[1], DEC)); Serial.print(".");
                                 Serial.print(String(ip[2], DEC)); Serial.print(".");
                                 Serial.print(String(ip[3], DEC)); Serial.println();

  delay(100);
  // Creating UDP server
  Serial.println(F("Creating HTTP server ..."));
  server.begin();
  Serial.println(F("HTTP server created"));
}

boolean alreadyConnected = false;

void connexionRead(){
  // listen for incoming clients
  WiFiClient client = server.available();
  if (client) {
    Serial.println("\nNew client.");
    // an http request ends with a blank line
    boolean currentLineIsBlank = true;
    int i = 0;
    while (client.connected()) {
      /*log(String(i));
      log(" Client is still connected ... There are ");
      log(String(client.available(), DEC));
      logln(" bytes available.");*/
      if (client.available()) {

        static char buffer[20];
        if(line(client.read(), buffer, 20) > 0){
          Serial.print(F("Line : ["));
          Serial.print(buffer);
          Serial.println(F("]"));
          char *command = strtok(buffer, " \t");
          Serial.print(F("Command : ["));
          Serial.print(command);
          Serial.println(F("]"));
          if(strcmp(command, "GET") == 0){
            char *path = strtok(NULL, " \t");
            Serial.print(F("The path is ["));
            Serial.print(path);
            Serial.println(F("]"));

            client.println(F("HTTP/1.1 200 OK"));
            client.println(F("Content-Type: text/html"));
            client.println(F("Connection: close"));  // the connection will be closed after completion of the response
            client.println(F("Refresh: 60"));  // refresh the page automatically every 60 sec
            client.println();
            client.println(F("<!DOCTYPE HTML>"));
            client.println(F("<html>"));

            if(strcmp(path, "/ping") == 0){
              client.println(F("Pong !"));
            }

            else if(strcmp(path, "/hello") == 0){
              client.println(F("Hello !<br/>I am a Stepper project,"));
              client.println(F(" created by Ivan CANET, Raphaël BOUCHY and Yannick COUSIN.<br/>"));
              client.println(F("<br/>I work with an Arduino Uno and an Arduino WiFi shield.<br/><br/>"));
              client.println(F("Here are all the commands that I know :<br/>"));
              client.println(F("<a href=\"ping\">Ping</a><br/>"));
              client.println(F("<a href=\"hello\">Hello</a><br/>"));
              client.println(F("<a href=\"current\">Current</a><br/>"));
            }

            else if(strcmp(path, "/current") == 0){
              int value = 0;
              do{
                client.println(value = readBuffer());
              }while(value != -1);
            }

            else{
              client.println(F("Error 404, this page doesn't exist."));
              Serial.print(F("Error 404 : ["));
              Serial.print(path);
              Serial.println(F("]"));
            }

            client.println(F("</html>"));
            Serial.println(F("Request fullfilled."));
          }
        }
        
        /*String line = client.readStringUntil("\n");
        Serial.print("Line : [");
        Serial.print(line);
        Serial.println("]");
        
        String cmd = line.substring(0, 3);
        Serial.print("Command : [");
        Serial.print(cmd);
        Serial.print("]");
        
        if(cmd.equals("GET")){
          Serial.println(" - valid.\n");
          String request = "";
          int index = 5;
          while(!String(line.charAt(index)).equals(String(' '))){
            request += String(line.charAt(index));
            index++;
          }
          Serial.print("Request : [");
          Serial.print(request);
          Serial.println("]");

          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connection: close");  // the connection will be closed after completion of the response
          client.println("Refresh: 60");  // refresh the page automatically every 60 sec
          client.println();
          client.println("<!DOCTYPE HTML>");
          client.println("<html>");
          
          if(request.equals("ping")){
            Serial.println("Command found : 'ping'.");
            client.println("Pong !");
          }
          
          else if(request.equals("current")){
            Serial.println("Command found : 'current'.");
            Serial.print("Data sent : ");
            Serial.println(getContent());
            client.println(getContent());
          }

          else{
            Serial.println("No command found, sending error 404.");
            client.println("404 Error.");
          }

          client.println("</html>");
          Serial.println("Finished processing the request ...\n");
        }else{
          Serial.println(" - not valid, waiting for next line.\n");
        }*/
        
        
        /*
        char c = client.read();
        log(String(c));
        // if you've gotten to the end of the line (received a newline
        // character) and the line is blank, the http request has ended,
        // so you can send a reply
        if (c == '\n' && currentLineIsBlank) {
          // send a standard http response header
          logln("Sending response ...");
          client.flush();
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connection: close");  // the connection will be closed after completion of the response
          client.println("Refresh: 60");  // refresh the page automatically every 60 sec
          client.println();
          client.println("<!DOCTYPE HTML>");
          client.println("<html>");
          // output the value of each analog input pin
          client.println(getContent());
          client.println("</html>");
          logln("Response sent.");
          delay(100);
          client.stop();
           break;
        }
        if (c == '\n') {
          // you're starting a new line
          currentLineIsBlank = true;
        } 
        else if (c != '\r') {
          // you've gotten a character on the current line
          currentLineIsBlank = false;
        }*/
      }
      i++;
      if(i > 1000){
        Serial.println(F("Too long to respond, closing connection."));
        break;
      }else if(client.available() == 0){
        Serial.println(F("Everything has been read, closing connection."));
        break;
      }
    }
    // give the web browser time to receive the data
    delay(100);
    
    // close the connection:
    client.flush();
    client.stop();
    Serial.println(F("client disonnected"));
  }
}



int line(int readch, char *buffer, int len){
  static int pos = 0;
  int rpos;
  if(readch > 0){
    switch(readch){
      case '\n': break;
      case '\r':
        rpos = pos;
        pos = 0;
        return rpos;
      default:
        if(pos < len-1){
          buffer[pos++] = readch;
          buffer[pos] = 0;
        }
    }
  }
  return -1;
}

/*int text(int readch, char *buffer, int len){
  static int posX = 0;
  int rpos;
  if(readch > 0){
    switch(readch){
      case '\n': break;
      case '\r':
      case ' ':
        rpos = pos;
        posX = 0;
        return rpos;
      default:
        if(posX < len-1){
          buffer[posX++] = readch;
          buffer[posX] = 0;
        }
    }
  }
  return -1;
}*/

