//#include <SD.h>

// macros from DateTime.h 
/* Useful Constants */
#define SECS_PER_MIN  (60UL)
#define SECS_PER_HOUR (3600UL)
#define SECS_PER_DAY  (SECS_PER_HOUR * 24L)

/* Useful Macros for getting elapsed time */
/*#define numberOfSeconds(_time_) (_time_ % SECS_PER_MIN)  
#define numberOfMinutes(_time_) ((_time_ / SECS_PER_MIN) % SECS_PER_MIN) 
#define numberOfHours(_time_) (( _time_% SECS_PER_DAY) / SECS_PER_HOUR)
#define elapsedDays(_time_) ( _time_ / SECS_PER_DAY)



*/int stepNumber = 0;/*
char filename[21];      // content/chanXXXX.txt
                        // ^      ^    ^   ^   ^
                        // 0      0    1   1   2
                        // 0      7    2   6   0
*/
/*void newFile(){

  Serial.println(F("Initializing SD card ..."));
  if(!SD.begin(sdPin)){
    Serial.println(F("Couldn't initialize SD card, stopping ..."));
    endProgram();
  }
  Serial.println(F("SD card ready !"));

  Serial.print(F("Does the 'content' dir exists ? "));
  if(!SD.exists("content")){
    Serial.println("No, Creating 'content' folder.");
    SD.mkdir("content");
  }else{
    Serial.println(F("Yes."));
  }
*//*
  char header[] = "content/chan";
  Serial.print(F("File name :   ["));
  for(int index = 0; index < 12; index++){
    filename[index] = header[index];
    Serial.print(filename[index]);
  }
  header[20] = '\0';
  Serial.print("XXXX");
  
  char footer[] = ".txt";
  for(int index = 0; index < 4; index++){
    filename[index + 16] = footer[index];
    Serial.print(footer[index]);
  }
  Serial.println(F("]"));
   
  int i1 = 0;
  int i2 = 0;
  int i3 = 0; 
  int i4 = 0;
  do{
    filename[12] = i4 + '0'; 
    filename[13] = i3 + '0';
    filename[14] = i2 + '0';
    filename[15] = i1 + '0';
    Serial.print(F("Trying file : ["));
    Serial.print(filename);
    Serial.print(F("], does it exists : "));
    Serial.println(SD.exists(filename) ? F("true") : F("false"));
    i1++;
    if(i1 >= 10)  {i1 = 0; i2++;}
    if(i2 >= 10)  {i2 = 0; i3++;}
    if(i3 >= 10)  {i3 = 0; i4++;}
    if(i4 >= 10)  {Serial.println(F("Too many files, closing.")); endProgram();}
  }while(SD.exists(filename));
  *//*
  Serial.print(F("Saving data into : ["));
  Serial.print(filename);
  Serial.println(F("]"));

  Serial.println(F("Creating current file ..."));
  File current = SD.open(filename, FILE_WRITE);

  if(!current){Serial.println(F("Couldn't open file...")); endProgram();}
  
  current.println(F("VERSION 1\nSERIES_NUMBER 1 Stepper\n"));
  current.println(F("NAMES Steps:Time"));
  current.println(F("UNITS Unit:ms\n"));
  current.close();
  Serial.println(F("File created and pre-filled."));
  */
  // Initialisation du contenu du fichier de sauvegarde
  /*content = "VERSION 1\nSERIES_NUMBER 1 Stepper\n\n";
  content += "NAMES Steps:Time\n";
  content += "UNITS Unit:ms\n";*/
//}

void newData(unsigned long time){
  // Enregistrer le nombre dans 'content'
  /*content += "\n";
  content += String(stepNumber, DEC);
  content += ":";
  content += String(time, DEC);*/

  // Ajouter le nombre au log
  Serial.print(String(stepNumber, DEC));
  Serial.print(":");
  Serial.println(String(time, DEC));

  writeBuffer(time);

  stepNumber++;
}

